describe Fastlane::Actions::AdmiraltoolsAction do
  describe '#run' do
    it 'prints a message' do
      expect(Fastlane::UI).to receive(:message).with("The admiraltools plugin is working!")

      Fastlane::Actions::AdmiraltoolsAction.run(nil)
    end
  end
end
